"""SIDLOC Ground Station Controller module main function"""
import sys

import sidlocgscontroller

if __name__ == '__main__':
    sys.exit(sidlocgscontroller.main())
