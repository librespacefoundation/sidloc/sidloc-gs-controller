"""Flowgraph module"""
import importlib
import logging
import sys

LOGGER = logging.getLogger(__name__)


class SidlocFlowgraph():
    """GNU Radio Flowgraph from module

    :param name: Flowgraph name
    :type name: str
    :param module_path: Path to flowgraph module
    :type module_path: Path
    :param parameters: Flowgraph parameters
    :type parameters: dict
    """

    def __init__(self, name, module_path, **parameters):
        """Class contructor"""
        self.name = name
        self.parameters = parameters
        module = SidlocFlowgraph._import_module(self.name, module_path)
        self.flowgraph = getattr(module, self.name)(**parameters)
        self.enabled = False

    @staticmethod
    def _import_module(name, location):
        if name in sys.modules:
            return sys.modules[name]

        spec = importlib.util.spec_from_file_location(name, location)
        module = importlib.util.module_from_spec(spec)
        sys.modules[name] = module
        spec.loader.exec_module(module)
        LOGGER.debug("Loaded flowgraph module '%s'.", name)
        return module

    @property
    def enabled(self):
        """Get flowgraph running status

        :return: Flowgraph running status
        :rtype: bool
        """
        if self.enabled:
            return True
        return False

    @enabled.setter
    def enabled(self, status):
        """Start or stop running of flowgraph

        :param status: Running status
        :type status: bool
        """
        if status:
            LOGGER.debug("Starting flowgraph '%s'.", self.name)
            self.flowgraph.start(65536)
        else:
            LOGGER.debug("Stopping flowgraph '%s'.", self.name)
            self.flowgraph.stop()

    def wait(self):
        """Wait for flowgraph to finish
        """
        LOGGER.debug("Waiting for flowgraph '%s' to finish.", self.name)
        self.flowgraph.wait()
        LOGGER.debug("Flowgraph '%s' finished.", self.name)
