"""Message module"""
import json
import logging
from importlib import resources
from pathlib import Path

from jsonschema import ValidationError, validate

from sidlocgscontroller import settings, topics

LOGGER = logging.getLogger(__name__)


class SidlocMessage():  # pylint: disable=too-few-public-methods, missing-class-docstring  # noqa: E501

    def __init__(self, mqttclient, controller, message):
        self.topic = str(
            Path(message.topic).relative_to(
                f'{settings.MQTT_TOP_LEVEL_TOPIC}/{mqttclient.username}'))
        self.payload = SidlocMessage._load(message.payload)
        self.controller = controller
        self.mqttclient = mqttclient
        self._topic_map = {
            'schedule': topics.schedule,
            'files': topics.files,
        }

    @staticmethod
    def _load(payload):
        if not payload:
            return None

        try:
            return json.loads(payload)
        except json.JSONDecodeError:
            LOGGER.error('Cannot parse JSON! Ignoring message...')
            return None

    def _validate(self):
        LOGGER.debug('Validating payload...')
        with resources.as_file(
                resources.files('sidlocgscontroller').joinpath(
                    settings.MESSAGE_SCHEMA_FILE)) as schema_file:
            try:
                json_schema_file = open(schema_file, 'r', encoding='utf-8')
            except FileNotFoundError:
                LOGGER.error("JSON schema '%s' not found!", schema_file)
                return False

            with json_schema_file:
                try:
                    json_schema = json.load(json_schema_file)
                except json.JSONDecodeError:
                    LOGGER.error(
                        'Cannot parse JSON schema! Ignoring message...')
                    return False

        try:
            validate(instance={self.topic: self.payload}, schema=json_schema)
        except ValidationError as error:
            LOGGER.exception(error)
            LOGGER.error('Invalid message, ignoring...')
            return False

        LOGGER.debug('Payload validated successfully!')
        return True

    def process(self):  # pylint: disable=missing-function-docstring
        LOGGER.debug("Processing paylod received on topic '%s'", self.topic)
        if not self.payload:
            LOGGER.debug('Empty payload! Ignoring...')
            return

        if self._validate():
            try:
                self._topic_map[self.topic](self)
            except KeyError:
                LOGGER.error("Unknown topic '%s'! Ignoring message...",
                             self.topic)
                return
