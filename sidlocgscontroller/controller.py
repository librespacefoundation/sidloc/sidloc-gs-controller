"""Controller module"""
import datetime
import json
import logging
import time

import paho.mqtt.client as mqtt

from sidlocgscontroller import jobs, settings
from sidlocgscontroller.message import SidlocMessage

from ._version import get_versions

__version__ = get_versions()['version']

del get_versions

LOGGER = logging.getLogger(__name__)

jobs.SCHEDULER.start()


class MQTTLogHandler(logging.Handler):
    """Logging handler to publish logs to MQTT
    """

    def __init__(self, client, topic, qos=2):
        self._client = client
        self._topic = topic
        self._qos = qos
        super().__init__()

    def emit(self, record):
        try:  # pylint: disable=too-many-try-statements
            message = self.format(record)
            self._client.publish(
                self._topic,
                json.dumps({
                    'timestamp':
                    datetime.datetime.now(tz=datetime.UTC).isoformat(),
                    'message':
                    message,
                }), self._qos)
        except Exception:  # pylint: disable=broad-exception-caught
            self.handleError(record)


class SidlocController():
    """Class to control and monitor SIDLOC Ground Station
    """

    def __init__(  # pylint: disable=too-many-arguments
            self,
            mqtt_host,
            mqtt_port,
            mqtt_username,
            mqtt_password,
            tls=False):
        self.flowgraph = None
        self.mqtt_host = mqtt_host
        self.mqtt_port = mqtt_port
        self.mqtt_client = mqtt.Client(
            callback_api_version=mqtt.CallbackAPIVersion.VERSION2,
            userdata=self)
        self.mqtt_log_handler = MQTTLogHandler(self.mqtt_client,
                                               f'sidloc/{mqtt_username}/logs')
        self.mqtt_client.username = mqtt_username
        self.mqtt_client.password = mqtt_password
        self.mqtt_client.on_connect = SidlocController._on_connect
        self.mqtt_client.on_message = SidlocController._on_message
        self.mqtt_client.on_disconnect = SidlocController._on_disconnect
        self.mqtt_client.on_connect_fail = SidlocController._on_connect_fail
        if tls:
            self.mqtt_client.tls_set()

    @staticmethod
    def _on_connect(client, _userdata, _flags, _reason_code, _properties):
        client.subscribe(
            f'{settings.MQTT_TOP_LEVEL_TOPIC}/{client.username}/schedule',
            qos=2)
        client.subscribe(
            f'{settings.MQTT_TOP_LEVEL_TOPIC}/{client.username}/files', qos=2)

        topic, payload = jobs.status('connected')
        client.publish(
            f'{settings.MQTT_TOP_LEVEL_TOPIC}/'
            f'{client.username}/{topic}',
            json.dumps(payload),
            qos=2)

        jobs.SCHEDULER.add_job(jobs.heartbeat,
                               trigger='interval',
                               args=[client],
                               id='heartbeat',
                               misfire_grace_time=30,
                               coalesce=True,
                               max_instances=1,
                               replace_existing=True,
                               minutes=1)

    @staticmethod
    def _on_message(client, userdata, message):

        sidloc_message = SidlocMessage(client, userdata, message)
        sidloc_message.process()

    @staticmethod
    def _on_connect_fail(_client, _userdata):
        LOGGER.error('Connection to MQTT server failed!')

    @staticmethod
    def _on_disconnect(_client, _userdata, _disconnect_flags, _reason_code,
                       _properties):
        LOGGER.error('Disconnected from MQTT server!')

    def start(self):  # pylint: disable=missing-function-docstring
        while True:  # pylint: disable=while-used
            try:
                self.mqtt_client.connect(host=self.mqtt_host,
                                         port=self.mqtt_port)
            except OSError as error:
                LOGGER.exception(error)
                time.sleep(10)
                continue
            break

        self.mqtt_client.loop_start()
        while True:  # pylint: disable=while-used
            time.sleep(10000)

    def stop(self):  # pylint: disable=missing-function-docstring
        jobs.disable_flowgraph(self.flowgraph)
        jobs.SCHEDULER.remove_all_jobs()
        if jobs.SCHEDULER.running:
            LOGGER.debug('Waiting for jobs to finish...')
            jobs.SCHEDULER.shutdown()
        self.mqtt_client.disconnect()
